import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { Route, Switch } from 'react-router-dom';

import routes from './routes';
import Sidebar from './components/Sidebar';

import './styles/App.scss';
import * as PropTypes from 'prop-types';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedCharacter: null,
        };
        this.props.dependencies.sseClient.listen();
    }
    getRoutes = routes => {
        return routes.map((prop, key) => {
            return (
                <Route
                    path={prop.path}
                    render={(props) => <prop.component
                        {...props}
                        {...prop.extraProps}
                        character={this.state.selectedCharacter}
                        dependencies={this.props.dependencies}
                    />}
                    key={key}
                />
            );
        });
    };

    selectCharacter(characterId) {
        console.log(`App.Character=${characterId}`);
        this.setState({ selectedCharacter: characterId });
    }

    render() {
        return (
            <Container className='App' id='esi-app' fluid>

                <div className='wrapper'>
                    <Sidebar
                        {...this.props}
                        routes={routes}
                        bgColor={'blue'}
                        selectCharacter={c => this.selectCharacter(c)}
                        dependencies={this.props.dependencies}
                    />
                    <div
                        className='main-panel'
                        ref='mainPanel'
                        data={this.state.backgroundColor}
                    >
                        <Switch>{this.getRoutes(routes)}</Switch>
                    </div>
                </div>
            </Container>
        );
    }
}

App.propTypes = {
    dependencies: PropTypes.object,
};

export default App;
