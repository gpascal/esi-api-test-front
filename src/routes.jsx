import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import GridLayout from './views/GridLayout';
import TradeReport from './components/TradeReport';
import TradeWatcher from './components/TradeWatcher';
import TradeOpportunities from './components/TradeOpportunities';

const routes = [
    {
        path: '/trade-reports',
        name: 'Trade reports',
        icon: <FontAwesomeIcon icon='money-check-alt' size='3x' />,
        component: GridLayout,
        extraProps: { panelType: TradeReport },
    },
    {
        path: '/trade-opportunities',
        name: 'Trade opportunities',
        icon: <FontAwesomeIcon icon='exchange-alt' size='3x' />,
        component: GridLayout,
        extraProps: { panelType: TradeOpportunities },
    },
    {
        path: '/watch-reports',
        name: 'Watch reports',
        icon: <FontAwesomeIcon icon='bell' size='3x' />,
        component: GridLayout,
        extraProps: { panelType: TradeWatcher, width: 6 },
    },
];

export default routes;
