import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';
import { Button, Card, CardBody, CardHeader, CardTitle, Table } from 'reactstrap';
import Sound from 'react-sound';
import moment from 'moment';
import * as _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'react-table/react-table.css';

import '../styles/EsiTable.scss';
import '../styles/TradeWatcher.scss';
import { SseEvent } from '../lib/SseClient';
import StationSelector from './StationSelector';
import { TypeIcon } from './TypeIcon';
import { Format } from '../lib/Format';
import notification from '../assets/audio/light.mp3';

class TradeWatcher extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestState: null,
            error: null,
            collapsed: false,
            selectedStation: null,
            data: null,
            acknowledged: [],
            now: moment(),
        };

        const sseClient = this.props.dependencies.sseClient;
        sseClient.on(SseEvent.TRADE_WATCHER_ERROR, error => this.setState({ error, requestState: null }));
        setInterval(() => this.setState({ now: moment() }), 1000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.character !== this.props.character) {
            this.setState({ selectedStation: null, data: null });
        }
    }

    // ===== Life Cycle ===== //

    toggleCollapsed() {
        this.setState({ collapsed: !this.state.collapsed });
    }

    // ===== XHR calls and callbacks ===== //

    startWatcher() {
        const { selectedStation } = this.state;
        const { dependencies: { apiClient, sseClient }, character } = this.props;
        const requestState = uuid();
        this.setState({ requestState });

        sseClient.on(SseEvent.TRADE_WATCHER_UPDATE, ({ stationId, characterId, trades, nextUpdate }) => {
            if (stationId === selectedStation) {
                window.trades = trades;
                console.log(`trades`, trades);
                this.setState({ data: trades, nextUpdate, acknowledged: [], playNotif: true });
                apiClient.bumpTradeWatcher(character, selectedStation);
            }
        });
        apiClient.startTradeWatcher(character, selectedStation, requestState)
            .catch(error => this.setState({ error }));
    }

    // ===== Render methods ===== //

    renderWatchTable() {
        const { data, nextUpdate, acknowledged, now, playNotif } = this.state;
        const { dependencies } = this.props;

        const dataToRender = _(data || [])
            .filter(d => !d.isBestSellOrder)
            .sortBy(['type.name'])
            .value();

        const nextUpdateIn = moment.duration(moment(nextUpdate).diff(now));

        return (
            <div className='TradeWatcher'>
                {nextUpdate && <div className='trade-watcher-next-update'>Next update {Format.shortDuration(nextUpdate, now)}</div>}
                {dataToRender.length > 0 && playNotif && <Sound
                    url={notification}
                    playStatus={Sound.status.PLAYING}
                    onFinishedPlaying={() => this.setState({ playNotif: false })}
                    volume={40}
                />}
                <Table className='trade-watcher-results'>
                    <thead />
                    <tbody>
                    {_.map(dataToRender, row => {
                        const rowAck = _.includes(acknowledged, row.type.type_id);
                        const updatableAt = row.sellOrders.length > 0 ? row.sellOrderUpdatableAt : row.buyOrderUdpatableAt;
                        const updatableIn = Format.shortDuration(updatableAt, now);
                        const adjustment = row.bestSellPrice - _.minBy(row.sellOrders, o => o.price).price - 0.01;
                        const adjustmentColor = adjustment > -.1 ? 'limegreen' : (adjustment <= -10 ? 'red' : 'orange');

                        return (
                            <tr key={row.type.type_id} className={`${rowAck ? 'acknowledged' : ''}`}>
                                <td style={{ maxWidth: 48 }}>
                                    <TypeIcon dependencies={dependencies} inEsiGrid typeId={row.type.type_id}/>
                                </td>
                                <td>{row.type.name}</td>
                                <td>
                                    {updatableIn ?
                                        <div>Updatable in {updatableIn}</div> :
                                        <div><FontAwesomeIcon icon='check' color='green' size='sm'/>&nbsp; Can be updated</div>}
                                </td>
                                <td>
                                    <span style={{ color: adjustmentColor}}>{Format.isk(adjustment)}</span>
                                </td>
                                <td>
                                    <Button
                                        disabled={rowAck}
                                        size='sm'
                                        onClick={() => this.setState({ acknowledged: [...acknowledged, row.type.type_id] })}
                                    >Ack</Button>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </Table>
            </div>
        );
    }

    render() {
        const { dependencies } = this.props;
        const { error, collapsed } = this.state;

        return (
            <Card className='TradeWatcher esi-panel'>
                <CardHeader className='esi-panel-header'>
                    <CardTitle tag='h2'>
                        Trade Watcher
                    </CardTitle>
                    <div className='esi-panel-header-buttons'>
                        <Button size='sm' color='link' onClick={() => this.toggleCollapsed()}>
                            <FontAwesomeIcon icon={collapsed ? 'expand' : 'minus'} color='orange' />
                        </Button>
                        <Button size='sm' color='link' onClick={() => this.props.close()}>
                            <FontAwesomeIcon icon='times' color='orange' />
                        </Button>
                    </div>
                </CardHeader>

                {error && <CardBody className='error'>
                    <div className='char-error'>{JSON.stringify(error)}</div>
                </CardBody>}

                <CardBody>
                    <StationSelector dependencies={dependencies} onChange={(stationId) => this.setState({ selectedStation: stationId })}/>
                    <Button disabled={Boolean(!this.state.selectedStation || this.state.data)} onClick={() => this.startWatcher()}>Start watcher</Button>

                    {this.renderWatchTable()}
                </CardBody>
            </Card>
        );
    }
}

TradeWatcher.propTypes = {
    dependencies: PropTypes.object,
    character: PropTypes.number,
    close: PropTypes.func,
};

export default TradeWatcher;
