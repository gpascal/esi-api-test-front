import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button } from 'reactstrap';

import '../styles/Characters.scss';
import CharacterCard from './CharacterCard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Characters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            data: [],
            error: null,
        }
    }

    componentDidMount() {
        const { sseClient } = this.props.dependencies;
        sseClient.on('character-updated', () => this.fetchCharacters());
        this.fetchCharacters();
    }

    fetchCharacters() {
        const { apiClient } = this.props.dependencies;
        apiClient.getCharacters()
            .then(data => this.setState({ data: data || [] }))
            .then(() => {
                const previouslySelectedId = localStorage.getItem('selected-character-id');
                return previouslySelectedId && this.selectCharacter(parseInt(previouslySelectedId));
            })
            .catch(err => this.setState({ error: err }));
    }

    refreshCharacter(characterId) {
        const { apiClient } = this.props.dependencies;
        return apiClient.refreshCharacter(characterId)
            .then(() => this.fetchCharacters());
    }

    deleteCharacter(characterId) {
        const { apiClient } = this.props.dependencies;
        apiClient.deleteCharacter(characterId)
            .then(() => this.fetchCharacters())
            .catch(err => {
                console.log(`Delete char error`, err);
                return this.setState({ error: err });
            });
    }

    selectCharacter(characterId) {
        this.setState({ selected: characterId });
        this.props.selectCharacter(characterId);
        localStorage.setItem('selected-character-id', characterId);
    }

    addCharacter() {
        const { apiClient } = this.props.dependencies;
        apiClient.newCharacter()
            .catch(err => this.setState({ error: err }));
    }

    render() {
        return (
            <div className='Characters'>
                {this.state.error && <div className='char-error'>{this.state.error.toString()}</div>}
                <ul className='char-list'>
                    {_.map(this.state.data || [], (character) => (
                        <CharacterCard
                            key={`character-${character.id}`}
                            character={character}
                            isSelected={(this.state.selected === character.id)}
                            selectCharacter={id => this.selectCharacter(id)}
                            refreshCharacter={id => this.refreshCharacter(id)}
                            deleteCharacter={id => this.deleteCharacter(id)}
                        />
                    ))}
                </ul>
                <Button
                    block
                    size='lg'
                    color='link'
                    className='char-add-button'
                    onClick={e => this.addCharacter(e)}
                >
                    <FontAwesomeIcon icon='plus' style={{ marginRight: '5px' }}/> Add a character
                </Button>
            </div>
        );
    }
}

Characters.propTypes = {
    config: PropTypes.object,
    portraitSize: PropTypes.oneOf([32, 64, 128, 256]),
    selectCharacter: PropTypes.func,
    dependencies: PropTypes.object,
};

export default Characters;
