import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardImg, CardText, CardTitle } from 'reactstrap';

import '../assets/css/nucleo-icons.css';
import '../styles/Characters.scss';
import { Format } from '../lib/Format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class CharacterCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }

    deleteCharacter(event) {
        this.props.deleteCharacter(this.props.character.id);
        event.stopPropagation();
    }

    refreshCharacter(event) {
        this.setState({ refreshing: true });
        this.props.refreshCharacter(this.props.character.id)
            .then(() => this.setState({ refreshing: false }));
        event.stopPropagation();
    }

    render() {
        const { isSelected, character: { id, portraits, data, walletBalance }, selectCharacter, style } = this.props;
        const { refreshing } = this.state;
        const img = isSelected
            ? <CardImg top width='100%' src={portraits.px256x256}/>
            : <div className='small-portrait-container'>
                <img alt='small-portrait' width={64} src={portraits.px64x64}/>
            </div>;

        return (
            <Card
                style={style}
                className={`Character-card ${isSelected ? 'character-card-selected' : ''}`}
                onClick={() => selectCharacter(id)}
            >
                {img}

                <CardBody>
                    <CardTitle className='char-name'>{data.name}</CardTitle>
                    <CardText className='wallet-balance'>{Format.isk(walletBalance, true)}</CardText>
                </CardBody>

                <div className='char-actions'>
                    <FontAwesomeIcon className={refreshing ? 'rotate' : ''} icon='sync-alt' onClick={(e) => this.refreshCharacter(e)} />
                    <FontAwesomeIcon icon='trash-alt' onClick={(e) => this.deleteCharacter(e)} />
                </div>
            </Card>
        );
    }
}

CharacterCard.propTypes = {
    isSelected: PropTypes.bool,
    selectCharacter: PropTypes.func,
    refreshCharacter: PropTypes.func,
    deleteCharacter: PropTypes.func,
    character: PropTypes.shape({
        id: PropTypes.number,
        portraits: PropTypes.shape({
            px32x32: PropTypes.string,
            px64x64: PropTypes.string,
            px128x128: PropTypes.string,
        }),
        data: PropTypes.shape({
            name: PropTypes.string,
        }),
        walletBalance: PropTypes.number,
    }),
};

export default CharacterCard;
