import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';
import { Button, Card, CardBody, CardHeader, CardTitle, Col, FormGroup, Input, Label, Row } from 'reactstrap';
import ReactTable from 'react-table';
import moment from 'moment';
import * as _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import RegionSelector from '../components/RegionSelector';
import { ColumnUtils, Format } from '../lib/Format';
import ProgressBar from './ProgressBar';
import { SseEvent } from '../lib/SseClient';

import 'react-table/react-table.css';
import '../styles/Characters.scss';
import '../styles/EsiTable.scss';
import '../styles/EsiForm.scss';
import '../styles/EsiPanel.scss';
import { MarketCompareFrame } from './MarketCompareFrame';

const View = {
    LIST: 'list',
    CREATE_FORM: 'createForm',
    VIEW_FORM: 'viewForm',
};

class TradeOpportunities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentView: View.LIST,
            allOpportunities: [],
            srcRegion: null,
            dstRegion: null,
            opened: {},
            requestState: null, // contains the id of the current async operation
            error: null,
            progress: [],
            p2: {},
            collapsed: false,
            options: {
                minBuyPrice: 20000,
                maxBuyPrice: 100000000,
                minTradedVolumePerDay: 30,
                minTradesPerDay: 5,
            },
            compareVisible: false,
            compareType: null,
            compareSrc: null,
            compareDst: null,
        };

        const { sseClient } = this.props.dependencies;
        sseClient.on(SseEvent.TRADE_OPPORTUNITIES_COMPUTED, report => this.handleOpportunitiesComputed(report));
        sseClient.on(SseEvent.TRADE_OPPORTUNITIES_UPDATED, report => this.fetchAllOpportunities());
        sseClient.on(SseEvent.TRADE_OPPORTUNITIES_PROGRESS, report => this.handleOpportunitiesProgress(report));
        sseClient.on(SseEvent.TRADE_OPPORTUNITIES_ERROR, error => this.setState({ error, requestState: null }));
    }

    componentDidMount() {
        if (this.props.character) {
            this.fetchAllOpportunities();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.character !== this.props.character) {
            this.setState({ opened: {}, allOpportunities: [], currentView: View.LIST });
            this.fetchAllOpportunities();
        }
    }

    // ===== Life Cycle ===== //

    toggleCollapsed() {
        this.setState({ collapsed: !this.state.collapsed });
    }

    // ===== XHR calls and callbacks ===== //

    fetchAllOpportunities() {
        const { dependencies: { apiClient }, character } = this.props;
        apiClient.getAllOpportunities(character)
            .then(allOpportunities => this.setState({ allOpportunities }))
            .catch(error => this.setState({ error }));
    }

    handleSubmit() {
        const { srcRegion, dstRegion, options } = this.state;
        const { dependencies: { apiClient } } = this.props;
        const requestState = uuid();
        this.setState({ requestState });

        apiClient.computeTradeOpportunities(srcRegion, dstRegion, options, requestState)
            .catch(error => this.setState({ error }));
    }

    handleOpportunitiesProgress({ state, stages }) {
        if (state === this.state.requestState) {
            this.setState({ progress: stages });
        }
    }

    handleOpportunitiesComputed({ state, ...data }) {
        if (state === this.state.requestState) {
            this.setState({
                allOpportunities: [...this.state.allOpportunities, data],
                opened: data,
                currentView: View.VIEW_FORM,
                requestState: null,
            });
        }
    }

    // ===== Render methods ===== //

    renderReportsList = () => {
        const { allOpportunities } = this.state;
        const { character, dependencies: { apiClient } } = this.props;
        return (
            <CardBody className='trade-opportunities-list'>
                <ReactTable
                    data={allOpportunities}
                    className='-striped -highlight esi-table'
                    columns={[
                        { accessor: 'date', Header: 'Date', Cell: row => moment(row.value).format('lll') },
                        { accessor: 'srcRegionName', Header: 'From region' },
                        { accessor: 'dstRegionName', Header: 'To region' },
                        { id: 'nbOpp', Header: 'Nb. opportunities', maxWidth: 100, accessor: d => d.opportunities.length },
                        {
                            id: 'actions', Header: 'Actions', maxWidth: 50,
                            accessor: d => ColumnUtils.actionIconsCell([
                                { iconClass: 'external-link-alt', callback: () => this.setState({ currentView: View.VIEW_FORM, opened: d }) },
                                { iconClass: 'trash-alt', callback: () => apiClient.deleteTradeOpportunity(d.id) },
                            ]),
                        },
                    ]}
                    defaultSorted={[
                        { id: 'date', desc: true },
                    ]}
                    defaultPageSize={10}
                />
                <Button disabled={!character} onClick={() => this.setState({ currentView: View.CREATE_FORM })}>
                    Compute new opportunities
                </Button>
            </CardBody>
        );
    };

    renderCreateForm = () => {
        const { srcRegion, dstRegion, requestState, progress, options } = this.state;
        const { minBuyPrice, maxBuyPrice, minTradedVolumePerDay, minTradesPerDay } = options;
        const { dependencies } = this.props;
        const isSubmitable = srcRegion && dstRegion && !requestState;

        return (
            <CardBody className='trade-opportunities-params'>
                <Row>
                    <Col xs={2}>
                        <FormGroup className='esi-form-group'>
                            <Label>Min buy price:</Label>
                            <Input
                                name='minBuyPrice'
                                type='number'
                                value={minBuyPrice}
                                onChange={e => this.setState({ options: { ...options, minBuyPrice: e.target.value } })}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Max buy price:</Label>
                            <Input
                                name='maxBuyPrice'
                                type='number'
                                value={maxBuyPrice}
                                onChange={e => this.setState({ options: { ...options, maxBuyPrice: e.target.value } })}
                            />
                        </FormGroup>
                    </Col>
                    <Col xs={2}>
                        <FormGroup>
                            <Label>Min traded volume/day:</Label>
                            <Input
                                name='minTradedVolumePerDay'
                                type='number'
                                value={minTradedVolumePerDay}
                                onChange={e => this.setState({ options: { ...options, minTradedVolumePerDay: e.target.value } })}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Min trades/day:</Label>
                            <Input
                                name='minTradesPerDay'
                                type='number'
                                value={minTradesPerDay}
                                onChange={e => this.setState({ options: { ...options, minTradesPerDay: e.target.value } })}
                            />
                        </FormGroup>
                    </Col>
                    <Col xs={2}>
                        <FormGroup className='esi-field-container'>
                            <Label>Source region:</Label>
                            <RegionSelector
                                className='esi-field esi-field-select'
                                disabled={!!requestState}
                                dependencies={dependencies}
                                onChange={r => this.setState({ srcRegion: r })}
                                value={this.state.srcRegion}
                            />
                        </FormGroup>
                        <FormGroup className='esi-field-container'>
                            <Label className='esi-field-label'>Destination region:</Label>
                            <RegionSelector
                                className='esi-field esi-field-select'
                                disabled={!!requestState}
                                dependencies={dependencies}
                                onChange={r => this.setState({ dstRegion: r })}
                                // value={this.state.dstRegion}
                            />
                        </FormGroup>
                    </Col>
                    <Col xs={12}>
                        <Button onClick={() => this.setState({ currentView: View.LIST })}>
                            Back to list
                        </Button>
                        <Button color='success' disabled={!isSubmitable} onClick={() => this.handleSubmit()}>
                            Compute opportunities
                        </Button>
                    </Col>
                </Row>

                {requestState && <CardBody>
                    <div>Computing trade opportunities... <i className='tim-icons icon-refresh-01 loader-rotate'/></div>
                    {_.map(progress, p => <ProgressBar center key={uuid()} progress={p}/>)}
                </CardBody>}
            </CardBody>
        );
    };

    renderReportForm = () => {
        const { opened } = this.state;
        const { dependencies: { config } } = this.props;
        const align = (textAlign = 'right') => row => <div style={{ textAlign }}>{row.value}</div>;
        return (
            <>
                <CardBody className='trade-report-form'>
                    {opened.options && <div className='trade-report-options'>
                        <div className='trade-report-options-title'>Options:</div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Buy region</div>
                            <div className='trade-report-option-value'>{opened.srcRegionName}</div>
                        </div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Sell region</div>
                            <div className='trade-report-option-value'>{opened.dstRegionName}</div>
                        </div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Min buy price</div>
                            <div className='trade-report-option-value'>{Format.isk(opened.options.minBuyPrice)}</div>
                        </div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Max buy price</div>
                            <div className='trade-report-option-value'>{Format.isk(opened.options.maxBuyPrice)}</div>
                        </div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Min vol/day</div>
                            <div className='trade-report-option-value'>{opened.options.minTradedVolumePerDay}</div>
                        </div>
                        <div className='trade-report-option'>
                            <div className='trade-report-option-label'>Min trades/day</div>
                            <div className='trade-report-option-value'>{opened.options.minTradesPerDay}</div>
                        </div>
                    </div>}
                    <ReactTable
                        className='-striped -highlight report-lines-table'
                        data={opened.opportunities}
                        columns={[
                            { id: 'typeIcon', Header: '', width: 35, accessor: d => <div className='grid-type-icon'><img alt='type-icon' src={`${config.imageApiUrl}/Type/${d.typeId}_32.png`} /></div> },
                            { id: 'typeName', Header: 'Type', width: 320, accessor: 'typeName', Cell: align('left') },
                            { accessor: 'tradeValue', Header: 'ROI', width: 50, Cell: ColumnUtils.numberCell },
                            {
                                Header: 'Buy region', columns: [
                                    { accessor: 'src.minSellPrice', Header: 'Buy at', width: 100, Cell: ColumnUtils.iskCell },
                                    { accessor: 'src.averageRecentPrice', Header: 'Average price (S)', width: 100, Cell: ColumnUtils.iskCell },
                                    { accessor: 'src.availableVolumeAt5Percent', Header: 'Available volume (price < 105%)(S)', width: 100, Cell: align() },
                                ],
                            },
                            {
                                Header: 'Sell region', columns: [
                                    { accessor: 'dst.minSellPrice', Header: 'Sell at', Cell: ColumnUtils.iskCell },
                                    { accessor: 'dst.averageRecentPrice', Header: 'Average price (D)', Cell: ColumnUtils.iskCell },
                                    { accessor: 'dst.availableVolumeAt5Percent', Header: 'Available volume (price < 105%)(D)', Cell: align() },
                                    { accessor: 'dst.tradedVolumeByDay', Header: 'Traded volume/d (D)', Cell: align() },
                                    { accessor: 'dst.tradesByDay', Header: 'Trades/d (D)', Cell: align() },
                                ],
                            },
                            {
                                id: 'actions', Header: 'Actions', maxWidth: 50,
                                accessor: d => ColumnUtils.actionIconsCell([
                                    { iconClass: 'columns', callback: () => console.log(`d=`,d) || this.setState({
                                            compareVisible: true,
                                            compareType: d.typeId,
                                            compareSrc: opened.srcRegionId,
                                            compareDst: opened.dstRegionId,
                                        }) },
                                ]),
                            },
                        ]}
                        defaultPageSize={10}
                        defaultSorted={[ { id: 'tradeValue', desc: true } ]}
                    />
                    <div style={{ textAlign: 'left' }}>
                        <Button onClick={() => this.setState({ currentView: View.LIST, opened: {} })}>
                            <FontAwesomeIcon icon='arrow-alt-circle-left'/> Back to report list
                        </Button>
                        <Button onClick={() => this.setState({
                            currentView: View.CREATE_FORM,
                            opened: {},
                            options: this.state.opened.options,
                            srcRegion: this.state.opened.srcRegionId,
                            dstRegion: this.state.opened.dstRegionId,
                        })}>
                            <FontAwesomeIcon icon='arrow-alt-circle-left'/> New opportunities from these options
                        </Button>
                    </div>
                </CardBody>
            </>
        );
    };

    render() {
        const { error, currentView, collapsed, compareType, compareSrc, compareDst } = this.state;

        return (
            <>
                <MarketCompareFrame
                    typeId={compareType}
                    region1={compareSrc}
                    region2={compareDst}
                    onClose={() => this.setState({ compareType: null, compareSrc: null, compareDst: null })}
                />
                <Card className='TradeReport esi-panel'>
                    <CardHeader className='esi-panel-header'>
                        <CardTitle tag='h2'>Trade Opportunities</CardTitle>
                        <div className='esi-panel-header-buttons'>
                            <Button size='sm' color='link' onClick={() => this.toggleCollapsed()}>
                                <FontAwesomeIcon icon={collapsed ? 'expand' : 'minus'} color='orange' />
                            </Button>
                            <Button size='sm' color='link' onClick={() => this.props.close()}>
                                <FontAwesomeIcon icon='times' color='orange' />
                            </Button>
                        </div>
                    </CardHeader>

                    {error && <CardBody className='error'>
                        <div className='char-error'>{error.toString()}</div>
                    </CardBody>}

                    {!collapsed && currentView === View.LIST && this.renderReportsList()}
                    {!collapsed && currentView === View.CREATE_FORM && this.renderCreateForm()}
                    {!collapsed && currentView === View.VIEW_FORM && this.renderReportForm()}
                </Card>
            </>
        );
    }
}

TradeOpportunities.propTypes = {
    dependencies: PropTypes.object,
    character: PropTypes.number,
    close: PropTypes.func,
};

export default TradeOpportunities;
