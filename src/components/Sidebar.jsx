import React from 'react';
import { NavLink } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import PerfectScrollbar from 'perfect-scrollbar';

import { Nav } from 'reactstrap';
import Characters from './Characters';

import '../styles/Sidebar.scss';

let ps;

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.activeRoute.bind(this);
    }
    // verifies if routeName is the one active (in browser input)
    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
    }
    componentDidMount() {
        if (navigator.platform.indexOf('Win') > -1) {
            ps = new PerfectScrollbar(this.refs.sidebar, {
                suppressScrollX: true,
                suppressScrollY: false
            });
        }
    }
    componentWillUnmount() {
        if (navigator.platform.indexOf('Win') > -1) {
            ps.destroy();
        }
    }
    linkOnClick = () => {
        document.documentElement.classList.remove('nav-open');
    };
    render() {
        const { bgColor, routes, selectCharacter, dependencies } = this.props;
        return (
            <div className='sidebar' data={bgColor}>
                <div className='sidebar-wrapper' ref='sidebar'>
                    <Characters
                        dependencies={dependencies}
                        selectCharacter={c => selectCharacter(c)}
                    />
                    <Nav>
                        {routes.map((prop, key) => {
                            if (prop.redirect) return null;
                            return (
                                <li className={this.activeRoute(prop.path)} key={key}>
                                    <NavLink
                                        to={prop.path}
                                        className='nav-link'
                                        activeClassName='active'
                                        onClick={this.props.toggleSidebar}
                                        style={{  }}
                                    >
                                        <span style={{ marginLeft: 5, marginRight: 15 }}>{prop.icon}</span>
                                        <span>{prop.name}</span>
                                    </NavLink>
                                </li>
                            );
                        })}
                    </Nav>
                </div>
            </div>
        );
    }
}

Sidebar.defaultProps = {
    bgColor: 'primary',
    routes: [{}]
};

Sidebar.propTypes = {
    bgColor: PropTypes.oneOf(['primary', 'blue', 'green']),
    routes: PropTypes.arrayOf(PropTypes.object),
    toggleSidebar: PropTypes.func,
    selectCharacter: PropTypes.func,
    dependencies: PropTypes.object,
};

export default Sidebar;
