import React from 'react';
import * as PropTypes from 'prop-types';
import { Button } from 'reactstrap';

export class MarketCompareFrame extends React.Component {
    render() {
        const { typeId, region1, region2 } = this.props;
        return (
            <div className={`iframe-cover ${typeId ? '' : 'iframe-cover-hidden'}`}>
                <div className='iframe-container'>
                    <iframe
                        title={`esi-evemarketer-iframe`}
                        className={`esi-iframe esi-iframe-${region2 ? 'left' : 'center'}`}
                        src={`https://evemarketer.com/regions/${region1}/types/${typeId}`}
                    />
                    {region2 && <iframe
                        title={`esi-evemarketer-iframe-2`}
                        className={`esi-iframe esi-iframe-right`}
                        src={`https://evemarketer.com/regions/${region2}/types/${typeId}`}
                    />}
                    <div className='iframe-actions'>
                        <Button onClick={() => this.props.onClose()}>Close</Button>
                    </div>
                </div>
            </div>
        );
    }
}

MarketCompareFrame.propTypes = {
    typeId: PropTypes.number,
    region1: PropTypes.number,
    region2: PropTypes.number,
    onClose: PropTypes.func,
};
