import { v4 as uuid } from 'uuid';

export const SseEvent = {
    CHARACTER_UPDATED: 'character-updated',
    CHARACTER_AUTH_ERROR: 'character-auth-error',

    TRADE_REPORTS_COMPUTED: 'trade-reports-computed',
    TRADE_REPORTS_ERROR: 'trade-reports-error',
    TRADE_REPORTS_PROGRESS: 'trade-reports-progress',
    TRADE_REPORTS_UPDATED: 'trade-reports-updated',

    TRADE_WATCHER_ERROR: 'trade-watcher-error',
    TRADE_WATCHER_PROGRESS: 'trade-watcher-progress',
    TRADE_WATCHER_UPDATE: 'trade-watcher-update',

    TRADE_OPPORTUNITIES_COMPUTED: 'trade-opportunities-computed',
    TRADE_OPPORTUNITIES_ERROR: 'trade-opportunities-error',
    TRADE_OPPORTUNITIES_PROGRESS: 'trade-opportunities-progress',
    TRADE_OPPORTUNITIES_UPDATED: 'trade-opportunities-updated',
};

export class SseClient {
    constructor(config) {
        this.config = config;
        this.listeners = {};
    }

    listen() {
        this.eventSource = new EventSource(`${this.config.apiUrl}/events`);
        this.eventSource.onmessage = (msg) => console.log(`SSE message received`, msg);
    }

    on(eventName, callback) {
        const listener = ({ data }) => {
            const parsed = JSON.parse(data);
            return callback(parsed);
        };
        const listenerId = uuid();
        this.listeners[listenerId] = listener;
        this.eventSource.addEventListener(eventName, listener);
        return listenerId;
    }

    un(eventName, listenerId) {
        const listener = this.listeners[listenerId];
        this.eventSource.removeEventListener(eventName, listener);
    }
}
