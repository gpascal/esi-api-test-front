import { ApiClient } from './ApiClient';
import { SseClient } from './SseClient';
import { DataStore } from './DataStore';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faAngleDoubleRight,
    faArrowAltCircleLeft,
    faBell,
    faCheck,
    faColumns,
    faExchangeAlt,
    faExpand,
    faExternalLinkAlt,
    faMinus,
    faMoneyCheckAlt,
    faPlus,
    faSyncAlt,
    faTimes,
    faTrashAlt,
    faWindowClose,
    faWindowMaximize,
    faWindowMinimize,
} from '@fortawesome/free-solid-svg-icons';

library.add(
    faAngleDoubleRight,
    faArrowAltCircleLeft,
    faBell,
    faCheck,
    faColumns,
    faExchangeAlt,
    faExpand,
    faExternalLinkAlt,
    faMinus,
    faMoneyCheckAlt,
    faPlus,
    faTimes,
    faSyncAlt,
    faTrashAlt,
    faWindowClose,
    faWindowMaximize,
    faWindowMinimize,
);

export class Dependencies {
    static build(config) {
        const apiClient = new ApiClient(config);
        const sseClient = new SseClient(config);
        const dataStore = new DataStore(config, apiClient);

        dataStore.initialize();

        return {
            config,
            apiClient,
            sseClient,
            dataStore,
        };
    }
}

